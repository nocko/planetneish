---
layout: page
title: About
tagline:
group: navigation
permalink: about.html
---
{% include JB/setup %}



My research interests focus on the use of orbital radar observations to study the **geology of planetary surfaces**, with a particular focus on processes related to **impact cratering**. Radar represents the best way to observe the surface of planets with large opaque atmospheres, such as Venus and Saturn's moon Titan, leading to a better understanding of their surface morphology. It also provides a wealth of information about the physical properties of the surface being imaged, revealing features not easily seen with optical data alone.


To pursue my interest in planetary radar, I am involved in several spacecraft missions.  I was a member of the Lunar Reconnaissance Orbiter's (LRO) Mini-RF science team, and an associate member of the Cassini RADAR team, which continues to provide the highest resolution views of Titan's surface.

Download my complete CV [here](/assets/Neish_CV_2022.pdf).

![Neish Lab 2016](/photos/IMG_3853.JPG)
*Neish Lab at Escape London (December 2016)*

### Education

* Ph.D., Planetary Sciences, University of Arizona (Aug. 2004 - Dec. 2008) 	    	

* B.Sc., Combined Honours Physics and Astronomy, University of British Columbia (Sept. 1999 - May 2004)

### Work Experience

* *Associate Professor*

  Department of Earth Sciences, The University of Western Ontario (July 2021 - present)

* *Assistant Professor*

  Department of Earth Sciences, The University of Western Ontario (July 2015 - June 2021)

* *Assistant Professor*

  Department of Physics and Space Sciences, Florida Institute of Technology (August 2013 - June 2015)

* *NASA Postdoctoral Fellow*

  Goddard Space Flight Center (July 2012 - July 2013)

* *Postdoctoral Fellow*

  The Johns Hopkins University Applied Physics Laboratory (May 2009 - June 2012)


### Awards

* Early Researcher Award, Ontario Ministry of Research, Innovation, and Science, 2017

* Minor Planet 16972 Neish, 2017

* AGU Ron Greeley Award, 2014

* NASA Postdoctoral Fellowship, 2012

* NASA Group Achievement Award to the Lunar Reconnaissance Orbiter (LRO) Team, 2010

* NSERC Postgraduate Scholarship - Doctoral, 2005-2008

* Julie Payette-NSERC Research Scholarship, 2004-2005
 

### Professional Affiliations

* American Geophysical Union
* Division for Planetary Science, American Astronomical Society

