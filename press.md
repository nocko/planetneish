---
layout: page
title: Press
tagline:
group: navigation
permalink: press.html
---
{% include JB/setup %}

**Press releases**

+ [Western Newsmakers 2019: Catherine Neish](https://news.westernu.ca/2019/12/newsmakers-2019-catherine-neish/)

+ [NASA chooses flying drone to explore surface of Titan, Saturn’s largest moon](https://globalnews.ca/news/5442091/nasa-dragonfly-mission-titan/)

+ [Where to look for life on Titan](https://www.airspacemag.com/daily-planet/where-look-life-titan-180969409/)

+ [Lava Flow Look-a-Likes](http://www.psrd.hawaii.edu/CosmoSparks/Jan17/lunar-impact-melt-flows.html)

+ [Swampy terrain may explain Titan's smooth complexion](http://news.sciencemag.org/space/2013/11/swampy-terrain-may-explain-titans-smooth-complexion)

+ [Soggy bogs swallow craters on Titan](http://www.newscientist.com/article/dn24430-astrophile-soggy-bogs-swallow-craters-on-titan.html#.UmKbgFMe47B)

+ [Titan Gets a Dune "Makeover"](http://www.nasa.gov/mission_pages/cassini/whycassini/titan-makeover.html)

+ [Saturn's largest moon undergoes crater makeover](http://www.cbc.ca/news/technology/story/2013/01/18/us-nasa-saturn-titan-moon.html)

+ [Life's Ingredients Could Form on Titan's Surface](http://www.space.com/10488-life-ingredients-form-titan-surface.html)

+ [Titan's Ice Volcanoes Might Produce Stuff of Life](http://www.space.com/5724-titan-ice-volcanoes-produce-stuff-life.html)

**Media**

+ [CBC's Quirks and Quarks: January 25, 2014](http://podcast.cbc.ca/mp3/podcasts/quirksaio_20140118_68423.mp3)

+ [DPS 2013: Tidbits from Titan](http://www.planetary.org/blogs/emily-lakdawalla/2013/10091509-dps-2013-tidbits-from-titan.html)

+ [Western Worlds Episode #110: Tempted by the Fates](http://cpsx.uwo.ca/news?post_id=6593)

+ [The Titanium Physicists Podcast: Episode 40](http://titaniumphysicists.brachiolopemedia.com/2013/12/25/episode-40-snow-lines-and-no-rhymes-with-taylor-mali/)

+ [The Titanium Physicists Podcast: Episode 28](http://titaniumphysicists.brachiolopemedia.com/2013/01/20/episode-28-smoke-like-swirls-on-the-moon-with-jordan-harbringer/)

+ [The Titanium Physicists Podcast: Episode 5](http://titaniumphysicists.brachiolopemedia.com/2011/12/23/episode-5-the-dunes-of-titan/)

+ [Video Clip from the Discovery Channel's Special "Are We Alone?"](http://dsc.discovery.com/tv-shows/other-shows/videos/are-we-alone-mysterious-black-sludge.htm)

**Interviews**

+ [NASA Solar System Exploration: People](http://solarsystem.nasa.gov/people/profile.cfm?Code=NeishC)

+ [Women in Planetary Science](http://womeninplanetaryscience.wordpress.com/2010/10/15/catherine-neish/)
