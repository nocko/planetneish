---
layout: page
title: Research Activities
permalink: research.html
tagline:
group: navigation
---
{% include JB/setup %}

In my lab at the University of Western Ontario, we use remote sensing to understand the geology of the solid surfaces in our solar system. Please click on the links below to see the most recent work of my students and postdocs.

**Neish Lab at The University of Western Ontario**

* [Jahnavi Shah](http://jahnavishah.com/), PhD Candidate
* [Leah Sacks](https://leahinspace.com), PhD Candidate
* [Cailin Gallinger](https://cgallinger.github.io/), PhD Candidate
* [Reid Perkins](https://reidperkins.github.io/reidsite/), PhD Candidate
* [Ashka Thaker](https://ashkathaker.com/), PhD Candidate
* [Sashank Vanga](https://sites.google.com/view/sashanks-space/home), PhD Candidate
* [Elisa Dong](https://abstract-ed.me/), PhD Candidate (co-supervised at York University)
* [Anthony Dicecca](https://adiceccageosciences.wordpress.com), MSc Candidate
* [Taylor Duncan](https://taylormarieduncan.wordpress.com/), MSc Candidate

**Neish Lab Alumni**

* [Ethan Schaefer](https://ethan-i-schaefer.webnode.com/), Postdoctoral Fellow, 2019-2021
* Michael Zanetti, Postdoctoral Fellow, 2015-2018
* Nora Weitz, Postdoctoral Fellow, 2017-2018
* Byung-Hun Choe, Postdoctoral Fellow, 2018

* [Josh Hedgepeth](https://jjoshh.com/), PhD 2022
* [Gavin Tolometti](https://gavintolometti.wixsite.com/gavinonthemoon), PhD 2021 (co-supervised)
* Byung-Hun Choe, PhD 2017 (co-supervised)

* [Rachel Yingling](https://www.willyingling.com/), MSc 2020 (co-supervised)
* [Carolina Rodriguez](http://carolinarsv.website2.me), MSc 2019
* [Alyssa Werynski](https://alyssascience.wordpress.com), MSc 2018
* [Jeff Daniels](https://jwdanielmsc.wordpress.com/), MSc 2018
* [Elise Harrington](http://elisescience.blogspot.ca/), MSc 2018

* [Sophia Trozzo](https://trozzosophia.editorx.io/myblog), Undergraduate Researcher 2021-2022
* Leah Davis-Purcell, Undergraduate Researcher 2021-2022
* [Samuel Gagnon](https://samuelgagnon04.wixsite.com/myblog), Undergraduate Researcher 2021
* [Émilie Laflèche](https://astroemilie.wordpress.com), Undergraduate Researcher 2020
* [Serenity Fan](https://blogs.ubc.ca/arsgeophysica/academics/), Undergraduate Researcher 2017
* [Maria Shaposhnikova](http://mariashaposhnikova.blogspot.ca/), Undergraduate Researcher 2016
* [Rachel Maj](http://blogs.ubc.ca/rachelmaj/), Undergraduate Researcher 2016

<br />

### Neish Lab 2022

![Neish Lab 2022](/photos/neish_lab_2022.jpg)

### Neish Lab 2021

![Neish Lab 2021](/photos/IMG_0218.jpg)

### Neish Lab 2020

![Neish Lab 2020](/photos/neish_lab_2020.jpg)

### Neish Lab 2019

![Neish Lab 2019](/photos/IMG_6247.JPG)

### Neish Lab 2018

![Neish Lab 2018](/photos/neish_lab_2018.JPG)

### Neish Lab 2017

![Neish Lab 2017](/photos/lab_dec_2017.jpg)

### Neish Lab 2016

![Neish Lab 2016](/photos/neish_lab_2016.jpg)