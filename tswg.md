---
layout: page
title: Titan Surface Working Meeting
tagline:
---
{% include JB/setup %}

> ### Florida Institute of Technology, Melbourne, FL ##
>
>
> ### January 27-28, 2014 ##

* **Schedule**
	
Current list of speakers:

	+ Barnes, J. "Wave activity detected on Punga Mare"
	+ Barnes, J. "Sand production and transport"
	+ Buratti, B. "The composition of the rain on Titan as determined by Cassini VIMS"
	+ Cottini, V. "Titan's surface temperature variations from Cassini CIRS"
	+ Hayes, A. "Lake/Sea formation, distribution, and volume"
	+ Hayes, A. "Orientation of Titan's dunes over diurnal, seasonal, and Milankovitch timescales"
	+ Horvath, D. "Subsurface control of lakes at low polar latitudes"
	+ Jennings, D. "Titan's seasonal surface temperatures"
	+ Kirk, R. "Titan topography update: Land of lakes" (virtual)
	+ Lopes, R. "Titan's blandlands: Are they massive sand sheets?"
	+ Lorenz, R. "Tides in Kraken"
	+ Lorenz, R. "Roter Kamm, Namibia: An Analog for Titan Dune:Crater Interaction"
	+ Mitchell, K. "Karstic processes on Titan and Earth"
	+ Neish, C. "Titan's crustal composition revealed by its impact craters"
	+ Sittler, E. "Titan's ionosphere, aerosol formation, and Titan's GCR Sub-surface Model and Exobiological Molecules"
	+ Sotin, C. "Titan's lakes: recent observations by VIMS"
	+ Turtle, E. "Titan's north polar deposit"
	+ Vixie, G. "Northern temperate lakes on Titan"

Please contact Catherine Neish (cneish@fit.edu) if you would like to volunteer to give a talk or present a poster.

* **Transportation**
 	    	
Participants are encouraged to book flights into Orlando (MCO), as it should be cheaper and faster than flying into Melbourne (MEL). Once in Orlando, you can get an inexpensive rental car and drive an hour east to Melbourne.  Several small tolls are required on the drive (amounting to ~$2), so be sure to have some cash on hand.  Alternatively, there are several shuttle services that can transport you from Orlando to Melbourne, i.e. [Melbourne Airport Express](http://www.melbourneairportexpress.com/orlando-airport-melbourne-airport-shuttle.html) or [Florida Shuttle Transportation](http://www.floridashuttletransportation.com/melbourne-shuttle-bus.html).

	[Map](http://goo.gl/maps/QAlHj)

* **Hotel**
		
A block of rooms is reserved at the Crowne Plaza on the oceanfront. Reservations can be made at 877-701-9252 or [http://www.cpmelbourne.com/](http://www.cpmelbourne.com/) using the group code "FLT".  (Note: This group code will be available on Oct. 30, and reservations must be made before Jan. 12.)  The rate for government employees is $103/night and the rate for non-government employees is $109/night.

* **Location**
		
The meeting will take place on the campus of the [Florida Institute of Technology] (http://www.fit.edu), on the 7th Floor of the Crawford Building (Building #29). A campus map is provided [here](http://www.fit.edu/visitors/maps/).  Visitor parking is available on the ground floor of the garage (Building #40). If these spots are full, I can provide a parking pass. Morning and afternoon coffee and snacks will be provided.  Participants will be responsible for their own lunches and dinners.  An all-you-can-eat buffet is available at the nearby Panther Dining Hall for $8.50/person. A group dinner will be arranged for Monday night at [The Mansion](http://www.thebigmansion.com/).

* **Nearby Sites of Interest**
		
Located on the Space Coast, Florida Tech is located just 40 miles south of Kennedy Space Center, featuring the new [Atlantis Shuttle Exhibit](http://www.kennedyspacecenter.com/the-experience/atlantis-shuttle-experience.aspx).  You can see a list of planned rockets launches and events [here](http://www.kennedyspacecenter.com/events.aspx).

* **Optional Karst Field Trip: January 26**
 	    	
There will be a optional one-day karst field trip for all interested participants. We will meet in Orlando at **7:30 am on Sunday, January 26**.  (Tentative meeting location: 7-Eleven near the airport on McCoy Road.) From there, it is a ~1.5 hour drive to western Florida, where we will investigate several dry and wet karst systems. We will visit [Peace and Vandal Caves](http://water.usgs.gov/ogw/karst/kigconference/abt_karstfeatures.htm) in Withlacoochee State Forest and [Gator Sink](http://pubs.usgs.gov/sir/2009/5140/pdf/sir2009-5140.pdf) near Bartow, FL. Karst expert Patty Metz from the USGS will be joining us at Gator Sink to help guide the discussion. Later that evening we will drive ~2.5 hours back to Melbourne. 

	NOTE: We will not be returning to Orlando, so please carpool so that we can reduce the number of cars in our caravan!  I will provide walkie-talkies for the road, and will distribute my cell phone number on Sunday morning.

	[Driving map](http://goo.gl/maps/PNLVf)

	Photos from the scouting trip:

    <ul class="thumbnails">
    <li class="span4">
    <div class="thumbnail">
    <img src="/assets/img/IMG_3476.JPG" alt="Gator Sink">
    <h4>Gator Sink</h4>  
    </div>
    </li>
    <li class="span4">
    <div class="thumbnail">
    <img src="/assets/img/IMG_3481.JPG" alt="Peace River">
    <h4>Peace River</h4>    
    </div>
    </li>
    <li class="span4">
    <div class="thumbnail">
    <img src="/assets/img/IMG_3485.JPG" alt="Vandal Cave">
    <h4>Vandal Cave</h4>    
    </div>
    </li>
    <li class="span4">
    <div class="thumbnail">
    <img src="/assets/img/IMG_3488.JPG" alt="Peace Cave">
    <h4>Peace Cave</h4>    
    </div>
    </li>
    </ul>
