---
layout: page
title: Publications
permalink: publications.html
tagline:
group: navigation
---
{% include JB/setup %}

**2022**

+ __Tolometti, G. D.__, **Neish, C.**, Hamilton, C., Osinski, G., Kukko, A., Voigt, J. (2022) Differentiating Fissure-Fed Lava Flow Types and Facies Using RADAR and LiDAR: An Example from the 2014-2015 Holuhraun Lava Flow-field. Journal of Geophysical Research – Solid Earth 127, e2021JB023419.

+ __Tolometti, G. D.__, Erickson, T. M., Osinski, G. R., Cayron, C., **Neish, C.** (2022) Hot Rocks: Constraining the Thermal Conditions of the Mistastin Lake Impact Melt Deposits Using Zircon Grain Microstructures. Earth and Planetary Science Letters 584, 117523.

+ Wakita, S., Johnson, B. C., Soderblom, J. M., __Shah, J.__, **Neish, C. D.** (2022) Methane-saturated layers limit the observability of impact craters on Titan. The Planetary Science Journal 3, 50.

+ __Hedgepeth, J.E.__, Buffo, J.J., Chivers, C.J., **Neish, C.D.**, Schmidt, B.E. (2022) Modeling the distribution of HCN in impact crater melt on Titan. The Planetary Science Journal 3, 51.

**2021**

+ **Neish, C.D.**, K. Cannon, L. Tornabene, R. Flemming, M. Zanetti, E. Pilles (2021) Spectral properties of lunar impact melt deposits from Moon Mineralogy Mapper (M3) data. Icarus 361, 114392.

+ Lev, E., Hamilton, C., Voigt, J. Stadermann, A., Zhan, Y., **Neish, C.** (2021) Emplacement conditions of lunar impact melt flows. Icarus 369, 114578.

+ Barnes, J. W. et al. (2021) Science Goals and Objectives for the Dragonfly Titan Rotorcraft Relocatable Lander. The Planetary Science Journal 2, 130.

+ __Schaefer, E.__, Hamilton, C., **Neish, C.**, Sori, M., Bramson, A., Beard, S (2021) Reexamining the potential to classify lava flows from the fractality of their margins. Journal of Geophysical Research – Solid Earth 126, e2020JB020949.

+ Morse, Z. R., G. R. Osinski, L. L. Tornabene, **C. Neish** (2021) Morphologic mapping and interpretation of ejecta deposits from Tsiolkovskiy Crater. Meteoritics and Planetary Science 56, 767-793.

+ Lorenz, R. D., MacKenzie, S. M., **Neish, C. D.**, Le Gall, A., Turtle, E. P., Barnes, J. W., Trainer, M. G., __Werynski, A.__, __Hedgepeth, J.__, and Karkoschka, E (2021) Selection and Characteristics of the Dragonfly Landing Site near Selk Crater, Titan. The Planetary Science Journal 2, 24.
+ __Rodriguez Sanchez-Vahamonde, C.__, **C. Neish** (2021) The emplacement style of Martian lava flows as inferred from their decimeter- and meter-scale roughness. The Planetary Science Journal 2, 15.

**2020**

+ Solomonidou, A., **C. Neish**, A. Coustenis, M. Malaska, A. Le Gall, R.M.C. Lopes, __A. Werynski__, K. Lawrence, N. Atlobelli, O. Witasse, A. Shoenfeld, C. Matsoukas, I. Baziotis, P. Drossart (2020) The chemical composition of impact craters on Titan: Implications for exogenic processing. Astronomy & Astrophysics 641, A16.

+ __Tolometti, G.__, **C. Neish**, G. Osinski, S. Hughes, S. Nawotniak (2020) Interpretations of lava flow properties from radar remote sensing data. Planetary and Space Science 190, 104991.

+ __J. Hedgepeth__, **C. Neish**, E. Turtle, B. Stiles, R. Kirk, R. Lorenz (2020) Titan’s impact crater population after Cassini. Icarus 344, 113664.

+ __B-H. Choe__, G.R. Osinski, **C.D. Neish**, L.L. Tornabene (2020) A modified semi-empirical radar scattering model for weathered rock surfaces. Canadian Journal of Remote Sensing 46, 1-14.

**2019**

+ __B-H. Choe__, G.R. Osinski, **C.D. Neish**, L.L. Tornabene (2019) Polarimetric SAR signatures for characterizing geological units in the Canadian Arctic. IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing 12, 4406-4414.

+ C. A. Griffith, P. F. Penteado, J. D. Turner, **C. D. Neish**, G. Mitri, N. J. Montiel, A. Schoenfeld, and R. M. C. Lopes (2019) Ice-rich corridor exposed on Titan's surface of organic sediments. Nature Astronomy 3, 643-648.

+__E. M. Harrington__, __M. Shaposhnikova__, **C. D. Neish**, L. L. Tornabene, G. R. Osinski, __B-H. Choe__, __M. Zanetti__ (2019) A polarimetric SAR and multispectral remote sensing approach for mapping salt diapirs: Axel Heiberg Island, NU, Canada. Canadian Journal of Remote Sensing 45, 54-72.

+ G. Wei, X. Li, H. Gan, D. Blewett, **C. Neish**, B. Greenhagen (2019) A new method for simulation of microwave brightness temperatures and recalibration of Chang’e-2 MRM data using thermal constraints from Diviner. Journal of Geophysical Research Planets 124, 1433-1450.

+ R. Lopes et al. (2019) [Titan as revealed by the Cassini RADAR](https://doi:10.1007/s11214-019-0598-6). Space Science Reviews 215, 33.

+ __A. Werynski__, **C. Neish**, A. Le Gall, M. Janssen (2019) [Compositional variations of Titan’s impact craters indicates active surface erosion](https://doi.org/10.1016/j.icarus.2018.12.007). Icarus 321, 508-521.

+ A. Hendrix et al. (2019) [The NASA Roadmap to Ocean Worlds](https://doi.org/10.1089/ast.2018.1955). Astrobiology 19, 1-27.

+ Y.-C. Zheng, K.L. Chan, K. T. Tsang, Y.-C. Zhu, G. P. Hu, D. T. Blewett, **C. Neish** (2019) [Analysis of Chang’E-2 brightness temperature data and production of high spatial resolution microwave maps of the Moon](https://doi.org/10.1016/j.icarus.2018.09.036). Icarus 319, 627-644.

+ A. Morrison, __M. Zanetti__, C. Hamilton, E. Lev, **C. Neish**, A. Whittington (2019) [Rheological investigation of lunar highland and mare impact melt simulants](https://doi.org/10.1016/j.icarus.2018.08.001). Icarus 317, 307-323.

**2018**

+ **C. D. Neish**, R. D. Lorenz, E. P. Turtle, J. W. Barnes, M. G. Trainer, B. Stiles, R. Kirk, C. A. Hibbitts, M. J. Malaska (2018) [Strategies for detecting biological molecules on Titan](https://www.liebertpub.com/doi/10.1089/ast.2017.1758). Astrobiology 18, 571-585.

+ G. R. Osinski, R. A. F. Grieve, J. E. Bleacher, **C. D. Neish**, E. A. Pilles, L. L. Tornabene (2018) [Igneous rocks formed by hypervelocity impact](https://doi.org/10.1016/j.jvolgeores.2018.01.015). Journal of Volcanology and Geothermal Research 353, 25-54.

+ V. J. Bray, C. Atwood-Stone, **C. D. Neish**, A. McEwen, N. Artemieva, J. N. McElwaine (2018) [Lobate impact melt flows within the extended ejecta blanket of Pierazzo crater](https://doi.org/10.1016/j.icarus.2017.10.002). Icarus 301, 26-36.

**2017**

+ **C. D. Neish**, R. R. Herrick, __M. Zanetti__, __D. Smith__ (2017) [The role of pre-impact topography in impact melt emplacement on terrestrial planets](https://doi.org/10.1016/j.icarus.2017.07.004z). Icarus 297, 240-251.

+ **C. D. Neish**, C. W. Hamilton, S. S. Hughes, S. Kobs Nawotniak, W. B. Garry, J. R. Skok, R. C. Elphic, E. Schaefer, L. M. Carter, J. L. Bandfield, G. R. Osinski, D. Lim, J. L. Heldmann (2017) [Terrestrial analogues for lunar impact melt flows](http://dx.doi.org/10.1016/j.icarus.2016.08.008). Icarus 281, 73-89.+ L.M. Carter, B.A. Campbell, **C.D. Neish**, M.C. Nolan, G.W. Patterson, J.R. Jensen, D.B.J. Bussey (2017) [A Comparison of Radar Polarimetry Data of the Moon from the LRO Mini-RF Instrument and Earth-based Systems](http://dx.doi.org/10.1109/TGRS.2016.2631144). IEEE Transactions on Geoscience and Remote Sensing 55, 1915-1927.

+ G.W. Patterson, A.M. Stickle, F.S. Turner, J.R. Jensen, D.B.J. Bussey, P. Spudis, R.C. Espiritu, R.C. Schulze, D.A. Yocky, D.E. Wahl, M. Zimmerman, J.T.S. Cahill, M. Nolan, L. Carter, **C.D. Neish**, R.K. Raney, B.J. Thomson, R. Kirk, T.W. Thompson, B.L. Tise, I.A. Erteza, C.V. Jakowatz (2017) [Bistatic Radar Observations of the Moon using Mini-RF on LRO and the Arecibo Observatory](http://dx.doi.org/10.1016/j.icarus.2016.05.017). Icarus 283, 2-19.

+ J. Bandfield, J.T. Cahill, L.M. Carter, **C.D. Neish**, G.W. Patterson, J.-P. Williams, and D.A. Paige (2017) [Distal ejecta from lunar impacts: Extensive regions of rocky deposits](http://dx.doi.org/10.1016/j.icarus.2016.05.013). Icarus 283, 282-299.**2016**

+ S.P.D. Birch, A.G. Hayes, W. Dietrich, A.D. Howard, C. Bristow, M.J. Malaska, J. Moore, M. Mastrogiuseppe, J.D. Hofgartner, D.A. Williams, O. White, J. Soderblom, J.W. Barnes, E. Turtle, J.I. Lunine, C. Wood, **C. Neish**, R. Kirk, E. Stofan, R. Lorenz, and R.M.C. Lopes (2016) [Geomorphologic mapping of Titan's polar terrains: Constraining surface processes and landscape evolution](http://dx.doi.org/10.1016/j.icarus.2016.08.003). Icarus 282, 214-236.+ S. Domagal-Goldman et al. (2016) [The Astrobiology Primer v2.0](http://online.liebertpub.com/doi/full/10.1089/ast.2015.1460). Astrobiology 16, 561- 653.
+ B. Greenhagen, **C.D. Neish**, J.-P. Williams, J. T. Cahill, R. R. Ghent, P. O. Hayne, S. J. Lawrence, N. E. Petro, J. L. Bandfield (2016) [Origin of anomalously rocky appearance of Tsiolkovskiy crater](http://dx.doi.org/10.1016/j.icarus.2016.02.041). Icarus 273, 237-247.

+ **C.D. Neish**, J.L. Molaro, J.M. Lora, A.D. Howard, R.L. Kirk, P. Schenk, V.J. Bray, R.D. Lorenz (2015) [Fluvial erosion as a mechanism for crater modification on Titan](http://dx.doi.org/10.1016/j.icarus.2015.07.022). Icarus 270, 114-129.

+ M.A. Janssen, A. Le Gall, R.M. Lopes, R.D. Lorenz, M.J. Malaska, A.G. Hayes, **C.D. Neish**, A. Solomonidou, K.L. Mitchell, J. Radebaugh, S.J. Keihm, M. Choukroun, C. Leyrat, P.J. Encrenaz, M. Mastrogiuseppe (2015) [Titan's surface at 2.18-cm wavelength imaged by the Cassini RADAR radiometer: Results and interpretations through the first ten years of observation](http://dx.doi.org/10.1016/j.icarus.2015.09.027). Icarus 270, 443-459.

+ M.J. Malaska, R.M. Lopes, D.A. Williams, **C.D. Neish**, A. Solomonidou, J.M. Soderblom, A.M. Schoenfeld, S.P. Birch, A.G. Hayes, A. Le Gall, M.A. Janssen, T.G. Farr, R.D. Lorenz, J. Radebaugh, and E.P. Turtle (2016) [Geomorphological map of the Afekan Crater region, Titan: Terrain relationships in the equatorial and mid-latitude regions](http://dx.doi.org/10.1016/j.icarus.2016.02.021). Icarus 270, 130-161.+ R.M.C. Lopes, M.J. Malaska, A. Solomonidou, A. Le Gall, M.A. Janssen, **C.D. Neish**, E.P. Turtle, S.P.D. Birch, A.G. Hayes, J. Radebaugh, A. Coustenis, A. Schoenfeld, B.W. Stiles, R.L. Kirk, K.L. Mitchell, E.R. Stofan, K.J. Lawrence (2016) [Nature, distribution, and origin of Titan’s undifferentiated plains](http://dx.doi.org/10.1016/j.icarus.2015.11.034). Icarus 270, 162-182.+ Z.Y.C. Liu, J. Radebaugh, E. H. Christiansen, R.A. Harris, **C.D. Neish**, R.L. Kirk, and R.D. Lorenz (2016) [The tectonics of Titan: Global structural mapping from Cassini RADAR](http://dx.doi.org/10.1016/j.icarus.2015.11.021). Icarus 270, 14-29.

**2015**

+ **C.D. Neish**, J. W. Barnes, C. Sotin, S. MacKenzie, J. M. Soderblom, S. Le Mouelic, R. L. Kirk, B. W. Stiles, M. J. Malaska, A. Le Gall, R. H. Brown, K. H. Baines, B. Buratti, R. N. Clark, P. D. Nicholson (2015) [Spectral properties of Titan's impact craters imply chemical weathering of its surface](http://dx.doi.org/10.1002/2015GL063824). Geophysical Research Letters, 42, doi:10.1002/2015GL063824.

**2014**

+ **C.D. Neish**, L.M. Carter (2014) [Planetary radar](http://dx.doi.org/10.1016/B978-0-12-415845-0.00053-0). In: Spohn, T., Johnson, T. (Eds.) Encyclopedia of the Solar System, 3rd Edition. Academic Press, London, UK, p. 1133-1159.

+ **C.D. Neish**, __J. Madden__, L.M. Carter, B.R. Hawke, T. Giguere, V.J. Bray, G.R. Osinski, J.T.S. Cahill (2014) [Global distribution of lunar impact melt flows](http://dx.doi.org/10.1016/j.icarus.2014.05.049). Icarus 239, 105-117.

+ H.J. Cleaves, **C. Neish**, M.P. Callahan, E. Parker, F.M. Fernandez, J.P. Dworkin (2014) [Amino acids generated from hydrated Titan tholins: Comparison with Miller-Urey electric discharge products](http://dx.doi.org/10.1016/j.icarus.2014.04.042). Icarus 237, 182-189.

+ **C.D. Neish**, R.D. Lorenz (2014) [Elevation distribution of Titan's craters suggests extensive wetlands](http://dx.doi.org/10.1016/j.icarus.2013.09.024). Icarus 228, 27-34.

**2013**

+ **C.D. Neish**, D.T. Blewett, J.K. Harmon, E.I. Coman, J.T.S. Cahill, C.M. Ernst (2013) [A comparison of rayed craters on the Moon and Mercury](http://dx.doi.org/10.1002/jgre.20166). Journal of Geophysical Research 118, doi:10.1002/jgre.20166.

+ R.D. Lorenz, B.W. Stiles, O. Aharonson, A. Lucas, A.G. Hayes, R.L. Kirk, H.A. Zebker, E.P. Turtle, F. Nimmo, **C.D. Neish**, J.W. Barnes, and E.R. Stofan (2013) [A global topographic map of Titan](http://dx.doi.org/10.1016/j.icarus.2013.04.002). Icarus 225, 367-377.

+ R.M.C. Lopes, R.L. Kirk, K.L. Mitchell, A. LeGall, J.W. Barnes, A. Hayes, J. Kargel, L. Wye, J. Radebaugh, E.R.  Stofan, M. Janssen, **C. Neish**, S. Wall, C.A. Wood, and J.I. Lunine (2013) [Cryovolcanism on Titan: New results from Cassini RADAR and VIMS](http://dx.doi.org/10.1002/jgre.20062). Journal of Geophysical Research 118, 1-20.

+ B. Shankar, G.R. Osinski, I. Antonenko, and **C.D. Neish** (2013) [A multispectral geological study of the Schrodinger impact crater](http://www.nrcresearchpress.com/doi/abs/10.1139/e2012-053). Canadian Journal of Earth Sciences 50, 44-63.

+ **C.D. Neish**, R.L. Kirk, R.D. Lorenz, V.J. Bray, P. Schenk, B. Stiles, E. Turtle, K. Mitchell, A. Hayes, and the Cassini RADAR Team (2013) [Crater topography on Titan: Implications for landscape evolution](http://dx.doi.org/10.1016/j.icarus.2012.11.030). Icarus 223, 82-90.

**2012**

+ S.W. Bell, B.J. Thomson, M.D. Dyar, **C.D. Neish**, J.T. Cahill, and D.B.J. Bussey (2012) [Dating small fresh craters with Mini-RF observations of ejecta blankets](http://dx.doi.org/10.1029/2011JE004007). Journal of Geophysical Research 117, E00H30.

+ J.E. Moores and 44 colleagues (2012) [A Mission Control Architecture for Lunar Sample Return as Field Tested in an Analogue Deployment to the Sudbury Impact Structure](http://dx.doi.org/10.1016/j.asr.2012.05.008). Advances in Space Research 50, 1666-1686.

+ **C.D. Neish**, L.M. Prockter, and G.W. Patterson (2012) [Observational constraints on the identification and distribution of chaotic terrain on icy satellites](http://dx.doi.org/10.1016/j.icarus.2012.07.009). Icarus 221, 72-79.

+ B.J. Thomson, D.B.J. Bussey, **C.D. Neish**, J.T.S. Cahill, E. Heggy, R.L. Kirk, G.W. Patterson, R.K. Raney, P.D. Spudis, T.W. Thompson, and E. Ustinov (2012) [An upper limit for ice in Shackleton crater as revealed by LRO Mini-RF orbital radar](http://dx.doi.org/10.1029/2012GL052119). Geophysical Research Letters 39, L14201. 

+ **C.D. Neish**, R.D. Lorenz (2012) [Titan's global crater population: A new assessment](http://dx.doi.org/10.1016/j.pss.2011.02.016). Planetary and Space Science 60, 26-33.

+ L.M. Carter, **C.D. Neish**, D.B.J. Bussey, P.D. Spudis, M.S. Robinson, G.W. Patterson, J.T. Cahill, R.K. Raney (2012) [Initial observations of lunar impact melts and ejecta flows with the Mini-RF radar](http://dx.doi.org/10.1029/2011JE003911). Journal of Geophysical Research, 117, E00H09.

**2011**

+ **C.D. Neish**, D.T. Blewett, D.B.J. Bussey, S.J. Lawrence, M. Mechtley, B.J. Thomson (2011) [The surficial nature of lunar swirls as revealed by the Mini-RF instrument](http://www.sciencedirect.com/science/journal/00191035). Icarus 215, 186-196.

+ **C.D. Neish** (2011) [News and Views: Titan's nitrogenesis](http://www.nature.com/ngeo/journal/v4/n6/abs/ngeo1162.html). Nature Geosciences 4, 356-357.

+ R.K. Raney, P. Spudis, B. Bussey, J. Crusan, J.R. Jensen, W. Marinelli, P. McKerracher, **C. Neish**, M. Palsetia, R. Schulze, H. Sequeira, H. Winters (2011) [The Lunar Mini-RF Radars: Hybrid Polarimetric Architecture and Initial Results](http://dx.doi.org/10.1109/JPROC.2010.2084970). Proceedings of the IEEE 99, 808-823.

+ **C.D. Neish**, D.B.J. Bussey, P. Spudis, W. Marshall, B.J. Thomson, G.W. Patterson, L.M. Carter (2011) [The nature of lunar volatiles as revealed by Mini-RF observations of the LCROSS impact site](http://www.agu.org/pubs/crossref/2011/2010JE003647.shtml). Journal of Geophysical Research 116, E01005.

**2010**

+ **C.D. Neish**, A. Somogyi, and M.A. Smith (2010) [Titan's primoridal soup: Formation of amino acids via low temperature hydrolysis of tholins](http://www.liebertonline.com/doi/abs/10.1089/ast.2009.0402). Astrobiology 10, 337-347.

+ **C.D. Neish**, R.D. Lorenz, R.L. Kirk, and L.C. Wye (2010) [Radarclinometry of the sand seas of Africa's Namibia and Saturn's moon Titan](http://dx.doi.org/10.1016/j.icarus.2010.01.023). Icarus 208, 385-394.

+ D.B.J. Bussey, J.A. McGovern, P.D. Spudis, **C.D. Neish**, H. Noda, Y. Ishihara, and S-A. Sorensen (2010) [Illumination conditions of the south pole of the Moon derived using Kaguya topography](http://dx.doi.org/10.1016/j.icarus.2010.03.028). Icarus 208, 558-564.

+ P.D. Spudis and 29 colleagues (2010) [Initial results for the north pole of the Moon from Mini-SAR, Chandrayaan-1 mission](http://dx.doi.org/10.1029/2009GL042259).  Geophysical Research Letters 37, L06204.

**2009**

+ **C.D. Neish**, A. Somogyi, J.I. Lunine, and M.A. Smith (2009) [Low temperature hydrolysis of laboratory tholins in ammonia-water solutions: Implications for prebiotic chemistry on Titan](http://dx.doi.org/10.1016/j.icarus.2009.01.003). Icarus 201, 412-421.

+ A. Coustenis, S.K. Atreya, T. Balint, R.H. Brown, M.K. Dougherty, F. Ferri, M. Fulchignoni, D. Gautier, R.A. Gowen, C.A. Griffith, L.I. Gurvits, R. Jaumann, Y. Langevin, M.R. Leese, J.I. Lunine, C.P. McKay, X. Moussas, I. M√ºller-Wodarg, F. Neubauer, T.C. Owen, F. Raulin, E.C. Sittler, F. Sohl, C. Sotin, G. Tobie, T. Tokano, E.P. Turtle, J.-E.Wahlund, J.H. Waite, K.H. Baines, J. Blamont, A.J. Coates, I. Dandouras, T. Krimigis, E. Lellouch, R.D. Lorenz, A. Morse, C.C. Porco, M. Hirtzig, J. Saur, T. Spilker, J.C. Zarnecki, E. Choi, **C.D. Neish**, and 111 colleagues (2009) [TandEM: Titan and Enceladus mission](http://dx.doi.org/10.1007/s10686-008-9103-z). Experimental Astronomy 23, 893-946.

**2008**

+ **C.D. Neish**, R.D. Lorenz, and R.L. Kirk (2008) [Radar topography of domes on planetary surfaces](http://dx.doi.org/10.1016/j.icarus.2008.03.013). Icarus 196, 552-564.

+ **C.D. Neish**, A. Somogyi, H. Imanaka, J.I. Lunine, and M.A. Smith (2008) [Rate measurements of the hydrolysis of organic polymers in cold aqueous solutions: Implications for prebiotic chemistry on the early Earth and Titan](http://dx.doi.org/10.1089/ast.2007.0193). Astrobiology 8, 273-287.

**2007**

+ R.M.C. Lopes, K.L. Mitchell, E.R. Stofan, J. I. Lunine, R. Lorenz, F. Paganelli, R. L. Kirk, C.A. Wood, S.D. Wall, L. Robshaw, A.D. Fortes, **C.D. Neish**, J. Radebaugh, E. Reffet, C. Elachi, G. Boubin, M. D. Allison, Y. Anderson, R. Boehmer, P. Callahan, P. Encrenaz, E. Flamini, G. Francescetti, Y. Gim, G. Hamilton, S. Hensley, M. A. Janssen, W. T. K. Johnson, K. Kelleher, D. O. Muhleman, G. Ori, R. Orosei, S. J. Ostro, G. Picardi, F. Posa, L. E. Roth, R. Seu, S. Shaffer, L. A. Soderblom, B. Stiles, S. Vetrella, R.D. West, L. Wye, and H. A. Zebker (2007) [Cryovolcanic features on Titan‚Äôs surface as revealed by the Cassini Radar Mapper](http://dx.doi.org/10.1016/j.icarus.2006.09.006). Icarus 186, 395-412.

**2006**

+ **C.D. Neish**, R.D. Lorenz, D.P. O'Brien, and the Cassini RADAR Team (2006) [The potential for prebiotic chemistry in the possible cryovolcanic dome Ganesa Macula on Titan](http://journals.cambridge.org/action/displayAbstract?fromPage=online&aid=457874&fulltextType=RA&fileId=S1473550406002898). The International Journal of Astrobiology 5, 57-65.

**2002**

+ P.C. Gregory, and **C.D. Neish** (2002) Density and velocity structure of the Be star equatorial disk in the binary LS I +61 303, a probable microquasar. The Astrophysical Journal 580, 1133-1148
