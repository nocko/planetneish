---
layout: default
title: Home
tagline: 
group: navigation
---
{% include JB/setup %}

<div id="myCarousel" class="carousel">
  <!-- Carousel items -->
  <div class="carousel-inner">
    <div class="item active">
      <img alt="" src="/assets/img/IMG_0696.jpeg">
      <div class="carousel-caption">
	<h4>Kelso Dunes</h4>
	<p>Mohave Desert, California</p>
      </div>
    </div>
    <div class="item">
      <img alt="" src="/assets/img/IMG_5468.jpeg">
      <div class="carousel-caption">
	<h4>Mt. Whitney</h4>
	<p>Sierra Nevada Range, California</p>
      </div>
    </div>
    <div class="item">
      <img alt="" src="/assets/img/IMG_2397.JPG">
      <div class="carousel-caption">
	<h4>Parque Nacional Tierra del Fuego</h4>
	<p>Patagonia, Argentina</p>
      </div>
    </div>
    <div class="item">
      <img alt="" src="/assets/img/IMG_1369.jpeg">
      <div class="carousel-caption">
	<h4>Geothermal Springs</h4>
	<p>Suðurnes, Iceland</p>
      </div>
    </div>
    <div class="item">
      <img alt="" src="/assets/img/IMG_5258.jpeg">
      <div class="carousel-caption">
	<h4>Half Moon Island</h4>
	<p>South Shetland Islands, Antarctica</p>
      </div>
    </div>
    <div class="item">
      <img alt="" src="/assets/img/IMG_0709.jpg">
      <div class="carousel-caption">
	<h4>Resolute Bay</h4>
	<p>Nunavut, Canada</p>
      </div>
    </div>
  </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
</div>




